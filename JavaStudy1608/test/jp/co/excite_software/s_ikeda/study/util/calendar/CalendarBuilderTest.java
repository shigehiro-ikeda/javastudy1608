package jp.co.excite_software.s_ikeda.study.util.calendar;

import static java.util.Calendar.*;

import java.io.IOException;
import java.io.PrintStream;
import java.util.GregorianCalendar;

public class CalendarBuilderTest {

    public static void main (String [] args) {

        try {
            if (args.length == 2) {
                int year = Integer.parseInt(args[0]);
                int month = Integer.parseInt(args[1]);

                testConsole(year, month);
            }
            else {
                GregorianCalendar calendar = new GregorianCalendar();
                int year = calendar.get(YEAR);
                int month = calendar.get(MONTH) + 1;

                testConsole(year, month);
                testConsole(year, ++month);
                testConsole(year, ++month);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 
     * @param args
     * @throws IOException 
     */
    public static void testConsole(int year, int month) throws IOException {

        System.out.println(CalendarBuilder.build(year, month, simpleText));
    }

    // TODO HTMLのTABLEとしてカレンダーを出力する CalendarOutput 実装を追加してください。

    /**  */
    static CalendarOutput simpleText = new CalendarOutputAdapter() {

        final String [] WEEK = { "日", "月", "火", "水", "木", "金", "土", };

        @Override
        public void outputYearAndMonth(PrintStream out, int year, int month) {
            out.printf("%d年%d月", year, month).println();
        }

        @Override
        public void outputWeekHeader(PrintStream out, int dayOfWeek) {
            out.printf(WEEK[dayOfWeek - 1]);
            out.printf(dayOfWeek == SATURDAY ? "" : " ");
        }

        @Override
        public void outputCloseWeekHeader(PrintStream out) {
            out.println();
        }

        @Override
        public void outputDate(PrintStream out, int year, int month, int dayOfMonth, int dayOfWeek, int monthOffset) {

            if (monthOffset == 0) {
                out.printf(dayOfWeek == SUNDAY ? "" : " ");
                out.printf("%2d", dayOfMonth);
            }
            else {
                out.printf(dayOfWeek == SUNDAY ? "  " : "   ");
            }
        }

        @Override
        public void outputCloseWeek(PrintStream out) {
            out.println();
        }
    };
}
