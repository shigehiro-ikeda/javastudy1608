package jp.co.excite_software.s_ikeda.study.util.calendar;

import java.io.PrintStream;

/**
 * カレンダー出力処理を実装するためのインターフェイスです。
 */
public interface CalendarOutput {

    /**
     * 年月の出力処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     * @param year 年
     * @param month 月
     */
    public void outputYearAndMonth(PrintStream out, int year, int month);

    /**
     * 曜日ヘッダの出力処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     * @param dayOfWeek 曜日 {@link java.util.Calendar.DAY_OF_WEEK}
     */
    public void outputWeekHeader(PrintStream out, int dayOfWeek);

    /**
     * 日の出力処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     * @param year 年
     * @param month 月
     * @param dayOfMonth 日
     * @param dayOfWeek 曜日 {@link java.util.Calendar.DAY_OF_WEEK}
     * @param monthOffset 指定した月ならば 0、前の月なら -1 次の月なら 1
     */
    public void outputDate(PrintStream out, int year, int month, int dayOfMonth, int dayOfWeek, int monthOffset);

    /**
     * 月表示の開始処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputOpenMonth(PrintStream out);

    /**
     * 月表示の終了処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputCloseMonth(PrintStream out);

    /**
     * 週ヘッダ表示の開始処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputOpenWeekHeader(PrintStream out);

    /**
     * 週ヘッダ表示の終了処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputCloseWeekHeader(PrintStream out);

    /**
     * 週表示の開始処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputOpenWeek(PrintStream out);

    /**
     * 週表示の終了処理を実装してください。
     * 
     * @param out このPrintSreamに対して出力してください
     */
    public void outputCloseWeek(PrintStream out);
}
