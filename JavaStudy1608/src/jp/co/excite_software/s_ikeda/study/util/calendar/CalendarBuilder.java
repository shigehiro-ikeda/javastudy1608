package jp.co.excite_software.s_ikeda.study.util.calendar;

import static java.util.Calendar.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class CalendarBuilder {

    /** System.lineSeparator() */
    private static final String LS = System.lineSeparator();
    /**  */
    private static final int[] WEEK = {
            SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY,
    };

    // TODO java.util.Calendar のインスタンスを受け取ってカレンダーを構築するメソッドをそれぞれ追加してください。
    public static String build(Calendar calendar, CalendarOutput output) {
        return null;
    }
    // TODO java.util.Date のインスタンスを受け取ってカレンダーを構築するメソッドをそれぞれ追加してください。
    public static String build(Date date, CalendarOutput output) {
        return null;
    }

    /**
     * 
     * @param year
     * @param month
     * @param output
     * @return
     * @throws IOException 
     */
    public static String build(final int year, final int month, CalendarOutput output) throws IOException {

        try (PipedInputStream pis = new PipedInputStream();
             PipedOutputStream pos = new PipedOutputStream();
             PrintStream out = new PrintStream(pos);) {

            pis.connect(pos);

            output.outputYearAndMonth(out, year, month);
            output.outputOpenWeekHeader(out);

            for (int w : WEEK) {
                output.outputWeekHeader(out, w);
            }
            output.outputCloseWeekHeader(out);

            GregorianCalendar calendar = new GregorianCalendar (year, month - 1, 1);
            calendar.add(DATE, 1 - calendar.get(DAY_OF_WEEK));

            output.outputOpenMonth(out);

            while (true) {
                output.outputOpenWeek(out);

                for (int w : WEEK) {
                    int y = calendar.get(YEAR);
                    int m = calendar.get(MONTH) + 1;
                    int d = calendar.get(DAY_OF_MONTH);
                    output.outputDate(out, y, m, d, w, m - month);

                    calendar.add(DATE, 1);
                }

                output.outputCloseWeek(out);

                if (calendar.get(MONTH) + 1 != month) {
                    break;
                }
            }

            output.outputCloseMonth(out);

            out.close();

            StringBuilder sb = new StringBuilder();
            try (BufferedReader br = new BufferedReader(new InputStreamReader(pis));) {
                String line = null;
                while ((line = br.readLine()) != null) {
                    sb.append(line + LS);
                }
            }

            return sb.toString();
        }
    }
}
