package jp.co.excite_software.s_ikeda.study.util.calendar;

import java.io.PrintStream;

public abstract class CalendarOutputAdapter implements CalendarOutput {

    @Override
    public void outputYearAndMonth(PrintStream out, int year, int month) {
    }

    @Override
    public void outputWeekHeader(PrintStream out, int dayOfWeek) {
    }

    @Override
    public void outputDate(PrintStream out, int year, int month, int dayOfMonth, int dayOfWeek, int monthOffset) {
    }

    @Override
    public void outputOpenMonth(PrintStream out) {
    }

    @Override
    public void outputCloseMonth(PrintStream out) {
    }

    @Override
    public void outputOpenWeekHeader(PrintStream out) {
    }

    @Override
    public void outputCloseWeekHeader(PrintStream out) {
    }

    @Override
    public void outputOpenWeek(PrintStream out) {
    }

    @Override
    public void outputCloseWeek(PrintStream out) {
    }

}
